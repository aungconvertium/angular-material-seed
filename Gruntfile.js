/**
 * Gruntfile
 * sass compiling and
 * javascript linting
 */

'use strict';

var js_files = [
  'Gruntfile.js', 
  'app/assets/js/*.js',
  'app/components/**/*.js',
  'app/pages/**/*.js',
  'e2e-tests/**/*.js'
];

module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    pug: {
      compile: {
        options: {
          client: false,
          pretty: true,
          data: {
            debug: false
          }
        },
        files: [
          {
            src: 'app/views/index.pug',
            dest: 'app/index.html'
          },
          {
            expand: true,
            cwd: 'app/views/pages/',
            src: ['**/*.pug'],
            dest: 'app/pages/',
            ext: '.html'
          }
        ]
      }
    },
    jshint: {
      files: js_files,
      options: {
        jshintrc: '.jshintrc'
      }
    },
    sass: {
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          'app/assets/css/app.css': 'app/sass/main.sass'
        }
      }
    },
    watch: {
      pug: {
        files: 'app/views/**/*.pug',
        tasks: ['pug']
      },
      css: {
        files: 'app/sass/**/*.sass',
        tasks: ['sass']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-pug');

  grunt.registerTask('lint', ['jshint']);
  grunt.registerTask('default', ['pug', 'sass', 'watch']);

};