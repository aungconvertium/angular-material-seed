'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ui.router',
  'ngMaterial',
  'myApp.page1',
  'myApp.page2',
  'myApp.version'
]).
config([
  '$locationProvider',
  '$stateProvider',
  '$urlRouterProvider',
  function($locationProvider, $stateProvider, $urlRouterProvider) {
    $locationProvider.hashPrefix('!');

    $urlRouterProvider.otherwise('/page1');
  }
])
.controller('navCtrl', function($scope, $state, $timeout) {
  // github.com/angular-ui/ui-router/issues/1627
  $timeout(function() {
    $scope.currentNavItem = $state.current.name;
  }, 10);
});
