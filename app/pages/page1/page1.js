'use strict';

angular.module('myApp.page1', [
  'ui.router',
  'ngMaterial'
])

.config([
  '$stateProvider',
  function($stateProvider) {
    $stateProvider.state('page1', {
      url: '/page1',
      templateUrl: 'pages/page1/page1.html',
      controller: 'page1Ctrl'
    });
  }
])

.controller('page1Ctrl', [function() {

}]);