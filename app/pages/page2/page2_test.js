'use strict';

describe('myApp.page2 module', function() {

  beforeEach(module('myApp.page2'));

  describe('page2 controller', function() {

    it('should ....', inject(function($controller) {
      //spec body
      var page2Ctrl = $controller('page2Ctrl');
      expect(page2Ctrl).toBeDefined();
    }));

  });
});