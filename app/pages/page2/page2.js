'use strict';

angular.module('myApp.page2', [
  'ui.router',
  'ngMaterial'
])

.config([
  '$stateProvider', 
  function($stateProvider) {
    $stateProvider.state('page2', {
      url: '/page2',
      templateUrl: 'pages/page2/page2.html',
      controller: 'page2Ctrl'
    });
  }
])

.controller('page2Ctrl', [function() {
  
}]);