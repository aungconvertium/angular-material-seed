'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {


  it('should automatically redirect to /page1 when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/page1");
  });


  describe('page1', function() {

    beforeEach(function() {
      browser.get('index.html#!/page1');
    });


    it('should render page1 when user navigates to /page1', function() {
      expect(element.all(by.css('ui-view p')).first().getText()).
        toMatch(/partial for page 1/);
    });

  });


  describe('page2', function() {

    beforeEach(function() {
      browser.get('index.html#!/page2');
    });


    it('should render page2 when user navigates to /page2', function() {
      expect(element.all(by.css('ui-view p')).first().getText()).
        toMatch(/partial for page 2/);
    });

  });
});
